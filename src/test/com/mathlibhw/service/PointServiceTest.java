package test.com.mathlibhw.service;

import main.com.mathlibhw.service.point.PointService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

public class PointServiceTest {

    PointService cut = new PointService();

    static Arguments[] checkPointTestArgs(){
        return new Arguments[]{
                Arguments.arguments(1, 1.3, 1),
                Arguments.arguments(1, -1.3, 1),
                Arguments.arguments(0, -1, 1.5),
                Arguments.arguments(1, 0, -1)
        };
    }

    @ParameterizedTest
    @MethodSource("checkPointTestArgs")
    void checkPointTest(int expected, double x, double y){
        int actual = cut.checkPoint(x, y);
        Assertions.assertEquals(expected, actual);
    }
}
