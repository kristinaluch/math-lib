package main.com.mathlibhw.service.formula;


public class FormulaService {


    public double getResult(double x){
        double u = Math.cos(4);
        double upIn = Math.sqrt(Math.pow(Math.E,(x+1))+2*Math.pow(Math.E,x)*Math.cos(x));
        double up = 6*(Math.log(upIn));
        double down = Math.log(x-(Math.pow(Math.E, (x+1))*Math.sin(x)));
        double plusUp = Math.cos(x);
        double plusDown = Math.pow(Math.E, Math.sin(x));
        double plus = Math.abs(plusUp/plusDown);
        double result = (up/down)+plus;

        if(Double.isNaN(result)||Double.isInfinite(result)){
            return result;
        }

        result = Math.round(result*1_000_000);
        result = result/1_000_000;
        return result;
    }
}
