package main.com.mathlibhw.service.car;

public class CarService {

    // d = s0+t(v1+v2)

    public double getDistanceCar(double speedCar1, double speedCar2, double startDistance, double time){
        double distance = startDistance + time*(speedCar1+speedCar2);
        distance = 0.01*(Math.round(distance*100));
        return distance;
    }
}
