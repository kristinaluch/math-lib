package test.com.mathlibhw.service;

import main.com.mathlibhw.service.shooting.ShootingService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

public class ShootingServiceTest {

    ShootingService cut = new ShootingService();

    static Arguments[] getDistanceRadiansTestArgs(){
        return new Arguments[]{
              Arguments.arguments(10.2, 36, 0.7854),
              Arguments.arguments(23.03, 72, 0.3),
              Arguments.arguments(100.98, 120, 0.55)

        };
    }

    @ParameterizedTest
    @MethodSource("getDistanceRadiansTestArgs")
    void getDistanceRadiansTest(double expected, double startingSpeed, double alpha){
        double actual = cut.getDistanceRadians(startingSpeed, alpha);
        Assertions.assertEquals(expected, actual);
    }
}
