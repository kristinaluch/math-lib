package main.com.mathlibhw.service.shooting;

public class ShootingService {

    // l = (v0^2 * sin2alpha)/g = (2*V0^2*sin(alpha)*cos(alpha))/g; g = 9,80665 �/�?

    private static final double g = 9.80665;
    private static final int metersInKm = 1000;
    private static final int secondsInHour = 3600;



    public double getDistanceDegrees(double startingSpeed, double alpha){
        alpha = Math.toRadians(alpha);
        return getDistanceRadians(startingSpeed, alpha);
    }
    public double getDistanceRadians(double startingSpeed, double alpha){
        double l;
        //��������� ��/� �� �/�
        startingSpeed = (startingSpeed*metersInKm)/secondsInHour;
        l = (2*Math.pow(startingSpeed,2)*Math.sin(alpha)*Math.cos(alpha))/g;
        //��������� �� 2 ������ ����� �����
        l = Math.round(l*100);
        l = l/100;
        return l;
    }
}
