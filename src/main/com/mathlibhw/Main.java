package main.com.mathlibhw;

import main.com.mathlibhw.service.formula.FormulaService;
import main.com.mathlibhw.service.shooting.ShootingService;

public class Main {
    public static void main(String[] args) {
        ShootingService shootingService = new ShootingService();
        System.out.println(shootingService.getDistanceDegrees(36, 45));
        System.out.println(shootingService.getDistanceRadians(36, 0.7854));
        FormulaService formulaService = new FormulaService();
        System.out.println(formulaService.getResult(4));

    }
}
