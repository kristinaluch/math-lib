package test.com.mathlibhw.service;

import main.com.mathlibhw.service.formula.FormulaService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import static java.lang.Double.NaN;

public class FormulaServiceTest {

    FormulaService cut = new FormulaService();

    static Arguments[] getResultTestArgs(){
        return new Arguments[]{
          Arguments.arguments(NaN, 1.3),
          Arguments.arguments(4.133_316, 4)
        };
    }

    @ParameterizedTest
    @MethodSource("getResultTestArgs")
    void getResultTest(double expected, double x){
        double actual = cut.getResult(x);
        Assertions.assertEquals(expected,actual);
    }
}
