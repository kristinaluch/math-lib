package main.com.mathlibhw.service.point;

public class PointService {
    private static final double[] TOP_OF_TRIANGLE_1_X ={0, 2, 0};
    private static final double[] TOP_OF_TRIANGLE_1_Y ={0, 2, -1};
    private static final double[] TOP_OF_TRIANGLE_2_X ={0, -2, 0};
    private static final double[] TOP_OF_TRIANGLE_2_Y ={0, 2, -1};


    public int checkPoint(double xPoint, double yPoint){
        boolean checkTriangle1 = checkInTriangle(xPoint, yPoint, TOP_OF_TRIANGLE_1_X, TOP_OF_TRIANGLE_1_Y);
        boolean checkTriangle2 = checkInTriangle(xPoint, yPoint, TOP_OF_TRIANGLE_2_X, TOP_OF_TRIANGLE_2_Y);
        if (checkTriangle1||checkTriangle2){
            return 1;
        }
        return 0;
    }
    private boolean checkInTriangle(double xPoint, double yPoint, double[] topX, double[] topY){
        double check1 = ((topX[0]-xPoint)*(topY[1]-topY[0]))-((topX[1]-topX[0])*(topY[0]-yPoint));
        double check2 = ((topX[1]-xPoint)*(topY[2]-topY[1]))-((topX[2]-topX[1])*(topY[1]-yPoint));
        double check3 = ((topX[2]-xPoint)*(topY[0]-topY[2]))-((topX[0]-topX[2])*(topY[2]-yPoint));
        boolean trueVar1 = check1>0&&check2>0&&check3>0;
        boolean trueVar2 = check1<0&&check2<0&&check3<0;
        boolean trueVar3 = check1==0||check2==0||check3==0;
        if (trueVar1||trueVar2||trueVar3){
            return true;
        }
        return false;
    }
}
