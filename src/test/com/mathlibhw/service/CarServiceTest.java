package test.com.mathlibhw.service;

import main.com.mathlibhw.service.car.CarService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

public class CarServiceTest {

    CarService cut = new CarService();

    static Arguments[] getDistanceCarTestArgs(){
        return new Arguments[]{
                Arguments.arguments(220, 60, 80, 10, 1.5),
                Arguments.arguments(85.29, 44.5, 61, 32.54, 0.5),
                Arguments.arguments(0, 0, 0, 0, 30),
                Arguments.arguments(5, 60, 40, 5, 0)
        };
    }
//double distance = startDistance + time*(speedCar1+speedCar2);
    @ParameterizedTest
    @MethodSource("getDistanceCarTestArgs")
    void getDistanceCarTest(double expected, double speedCar1, double speedCar2, double startDistance, double time){
        double actual = cut.getDistanceCar(speedCar1, speedCar2, startDistance, time);
        Assertions.assertEquals(expected, actual);
    }
}
